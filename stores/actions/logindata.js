export const SET_DATA = "SET_DATA";

export const loginCredentials = (email, password) => {
  return async (dispatch) => {
    const response = await fetch("http://cm.wemakeiot.net:3200/login", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        username: email,
        password: password,
        GrantType: "credentials",
      }),
    });

    const resData = await response.json();

    dispatch({
      type: SET_DATA,
      userId: resData._id,
      token: resData.token,
      message: resData.msessage,
    });
  };
};
