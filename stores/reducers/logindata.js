import { SET_DATA } from "../actions/logindata";

const initialState = {
  userId: null,
  token: null,
  message: null,
};

const logindataReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_DATA:
      return {
        userId: action.userId,
        token: action.token,
        message: action.msessage,
      };

    default:
      return state;
  }
};

export default logindataReducer;
