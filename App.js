import React from "react";
import { useSelector } from "react-redux";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import LoginScreen from "./screens/LoginScreen";
import SuccessfulScreen from "./screens/SuccessfulScreen";
import ProfileScreen from "./screens/ProfileScreen";
import LogOutScreen from "./screens/LogOutScreen";
import ReduxThunk from "redux-thunk";

import { Provider } from "react-redux";
import { createStore, combineReducers, applyMiddleware } from "redux";

import logindataReducer from "./stores/reducers/logindata";
import { Icon } from "react-native-elements";

const rootReducer = combineReducers({
  logindata: logindataReducer,
});
const store = createStore(rootReducer, applyMiddleware(ReduxThunk));

const Navigation = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

function TabHome() {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="Home"
        component={SuccessfulScreen}
        options={{
          headerTitleAlign: "center",
          headerStyle: { backgroundColor: "#fabd05" },
          tabBarIcon: ({ color, size }) => (
            <Icon name="home" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          headerTitleAlign: "center",
          headerStyle: { backgroundColor: "#fabd05" },
          tabBarIcon: ({ color, size }) => (
            <Icon name="person" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Log Out"
        component={LogOutScreen}
        options={{
          headerShown: false,
          tabBarIcon: ({ color, size }) => (
            <Icon name="logout" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
const AppWrapper = () => {
  const authentication = useSelector((state) => state.logindata.token);

  return (
    <Provider store={store}>
      <NavigationContainer>
        <Navigation.Navigator>
          <Navigation.Screen
            name="Login"
            component={LoginScreen}
            options={{ headerShown: false }}
          />
          {authentication && (
            <Navigation.Screen
              name="TabHome"
              component={TabHome}
              options={{ headerShown: false }}
            />
          )}
        </Navigation.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default function App() {
  return (
    <Provider store={store}>
      <AppWrapper />
    </Provider>
  );
}
