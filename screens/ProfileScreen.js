import React, { useRef } from "react";
import {
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
} from "react-native";
import * as Animatable from "react-native-animatable";

const ProfileScreen = (props) => {
  const swingAnimation = useRef();
  const { UserEmail } = props.route.params;

  return (
    <View style={styles.container}>
      <View style={styles.card}>
        <View style={styles.imageContainer}>
          <Image
            style={styles.image}
            source={{
              uri: "https://cdn0.iconfinder.com/data/icons/basic-11/97/34-512.png",
            }}
          />
        </View>
        <View style={styles.formStyle}>
          <View>
            <Text style={styles.textStyle}>Email</Text>
            <TextInput
              name="email"
              style={styles.inputStyle}
              defaultValue={UserEmail}
            />
          </View>
        </View>
        <Animatable.View ref={swingAnimation}>
          <TouchableOpacity
            style={styles.roundedButton}
            onPress={() => {
              if (true) {
                swingAnimation.current.swing(800);
              }
            }}
          >
            <Text style={styles.buttonText}>Change Password</Text>
          </TouchableOpacity>
        </Animatable.View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#edd491",
    justifyContent: "center",
    alignItems: "center",
    height: "100%",
  },
  card: {
    width: "85%",
    height: 500,
    backgroundColor: "grey",
    elevation: 5,
    borderRadius: 20,
    marginBottom: 50,
  },
  imageContainer: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 10,
  },
  image: {
    width: 100,
    height: 100,
  },
  formStyle: {
    marginVertical: 20,
    marginHorizontal: 20,
    padding: 10,
  },
  inputStyle: {
    borderColor: "orange",
    borderBottomWidth: 1,
    color: "white",
    fontSize: 16,
    letterSpacing: 3,
  },
  textStyle: {
    marginTop: 30,
    marginBottom: 5,
    color: "white",
    fontSize: 20,
    fontWeight: "bold",
  },
  roundedButton: {
    display: "flex",
    alignItems: "center",
    marginVertical: 70,
    marginHorizontal: 50,
    paddingVertical: 20,
    backgroundColor: "orange",
    borderRadius: 1000,
    elevation: 2,
  },
  buttonText: {
    color: "black",
    fontSize: 16,
    fontWeight: "bold",
  },
});

export default ProfileScreen;
