import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { StatusBar } from "expo-status-bar";
import { Formik } from "formik";
import * as yup from "yup";
import { StyleSheet, Text, View, Image, Alert } from "react-native";
import { Button, TextInput } from "react-native-paper";

import { SocialIcon } from "react-native-elements/dist/social/SocialIcon";

import * as logindataActions from "../stores/actions/logindata";

const LoginScreen = (props) => {
  const [isSet, setIsSet] = useState(true);

  const validationHandler = yup.object().shape({
    email: yup
      .string()
      .email("Please enter A Valid Mail id")
      .required("Email id is required"),
    password: yup
      .string()
      .min(8, `Password must have 8 characters`)
      .required("Password is required"),
  });

  const onSubmitHandler = async (values, actions) => {
    try {
      await dispatch(
        logindataActions.loginCredentials(values.email, values.password)
      );
    } catch (error) {
      console.log(error);
    }
    props.navigation.navigate("TabHome", {
      screen: "Profile",
      params: {
        UserEmail: values.email,
        UserPass: values.password,
      },
    });
    actions.resetForm();
  };

  const dispatch = useDispatch();

  return (
    <View style={styles.LoginContainer}>
      <Image
        style={styles.imgContainer}
        source={{
          uri: "https://www.pinclipart.com/picdir/big/51-511102_design-free-logo-srj-hd-logo-png-clipart.png",
        }}
      />
      <View>
        <Text style={styles.loginText}>Welcome !</Text>
      </View>

      <View style={styles.userInputContainer}>
        <Text style={{ marginTop: 20, fontSize: 24 }}>LOGIN</Text>
        <Formik
          validationSchema={validationHandler}
          initialValues={{ email: "", password: "" }}
          onSubmit={onSubmitHandler}
        >
          {({
            handleChange,
            values,
            handleSubmit,
            handleBlur,
            errors,
            isValid,
            touched,
          }) => (
            <>
              <View style={styles.inputFieldsContainer}>
                <TextInput
                  style={styles.textInput}
                  name="email"
                  placeholder="Enter user name"
                  onChangeText={handleChange("email")}
                  onBlur={handleBlur("email")}
                  value={values.email}
                  keyboardType="email-address"
                />
                {errors.email && touched.email && (
                  <Text style={{ fontSize: 10, color: "red" }}>
                    {errors.email}
                  </Text>
                )}

                <TextInput
                  style={styles.textInput}
                  name="password"
                  placeholder="Enter password"
                  onChangeText={handleChange("password")}
                  onBlur={handleBlur("password")}
                  value={values.password}
                  secureTextEntry={isSet}
                  right={
                    <TextInput.Icon
                      name={isSet ? "eye-off" : "eye"}
                      onPress={() => {
                        setIsSet((preValue) => !preValue);
                      }}
                    />
                  }
                />
                {errors.password && touched.password && (
                  <Text style={{ fontSize: 10, color: "red" }}>
                    {errors.password}
                  </Text>
                )}
                <Text
                  style={styles.forgottenText}
                  onPress={() => {
                    console.log("Forgotten");
                  }}
                >
                  Forgot Password ?
                </Text>
              </View>

              <View style={styles.buttonConatiner} disabled={!isValid}>
                <Button
                  style={styles.align}
                  onPress={handleSubmit}
                  color="#fac37a"
                  mode="contained"
                  disabled={!isValid}
                >
                  LOGIN
                </Button>
              </View>
              <Text style={{ margin: 20, marginTop: 35 }}>
                ---------------------------------Or login
                with-------------------------------
              </Text>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                }}
              >
                <SocialIcon type="google" />
                <SocialIcon type="facebook" />
                <SocialIcon type="twitter" />
                <SocialIcon type="linkedin" />
              </View>
              <View style={{ marginTop: 50 }}>
                <Text>
                  Dont't have an account ?
                  <Text
                    style={{ fontSize: 18, color: "purple" }}
                    onPress={() => {
                      console.log("Sign Up");
                    }}
                  >
                    Sign Up
                  </Text>
                </Text>
              </View>
            </>
          )}
        </Formik>
      </View>

      <StatusBar style="auto" backgroundColor="white" />
    </View>
  );
};
const styles = StyleSheet.create({
  LoginContainer: {
    backgroundColor: "#f2d56b",
    alignItems: "center",
    justifyContent: "center",
  },
  userInputContainer: {
    width: "100%",
    height: "80%",
    elevation: 20,
    backgroundColor: "white",
    marginTop: 30,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
    alignItems: "center",
    marginHorizontal: 10,
  },
  textInput: {
    height: 50,
    width: 320,
    marginTop: 20,
    backgroundColor: "white",
  },
  loginText: {
    fontSize: 18,
    textAlign: "center",
    fontWeight: "bold",
    letterSpacing: 12,
    paddingTop: 15,
  },
  align: {
    marginHorizontal: 11,
  },
  inputFieldsContainer: {
    marginTop: 20,
  },
  buttonConatiner: {
    marginTop: 10,
    borderRadius: 20,
    width: "90%",
    height: 40,
  },
  forgottenText: {
    color: "purple",
    textAlign: "right",
    margin: 10,
  },
  imgContainer: {
    width: 80,
    height: 80,
    marginTop: 80,
  },
});

export default LoginScreen;
