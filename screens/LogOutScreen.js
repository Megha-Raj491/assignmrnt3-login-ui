import React from "react";
import { StyleSheet, View, Button, Text } from "react-native";

const LogOutScreen = (props) => {
  return (
    <View style={styles.container}>
      <View style={styles.Card}>
        <Text style={styles.textStyle}>Are you sure, you want to logout</Text>
        <View style={styles.cardContainer}>
          <Button
            title="Cancel"
            color="orange"
            onPress={() => {
              props.navigation.navigate("Home");
            }}
          />
          <Button
            title="Log Out Now"
            color="orange"
            onPress={() => {
              props.navigation.navigate("Login");
            }}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#f0b85d",
  },
  Card: {
    width: "90%",
    height: 170,
    backgroundColor: "grey",
    paddingTop: 30,
    borderRadius: 20,
    elevation: 10,
  },
  cardContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  textStyle: {
    textAlign: "center",
    fontSize: 20,
    paddingBottom: 25,
    color: "orange",
    lineHeight: 30,
  },
});

export default LogOutScreen;
