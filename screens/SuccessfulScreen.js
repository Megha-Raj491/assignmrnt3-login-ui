import React from "react";
import { StyleSheet, View, Text, SectionList } from "react-native";

const ListData = [
  {
    title: "Web Developement",
    data: ["React", "Angular", "Andorid", "WordPress", "Flutter"],
  },
  {
    title: "Front End Languages",
    data: ["HTML", "CSS", "JavaScript"],
  },
  {
    title: "Back End Frameworks",
    data: ["Express", "Rails", "Laravel", "Spring", "Django"],
  },
  {
    title: "Back End Languages",
    data: ["PHP", "Java", "Python", "JavaScript", "Node.js"],
  },
];

const SuccessfulScreen = (props) => {
  const renderItem = ({ item, index }) => {
    return <Text style={styles.listItem}>{item}</Text>;
  };
  const sectionHeader = ({ section }) => {
    return <Text style={styles.headerItems}>{section.title}</Text>;
  };

  return (
    <View style={styles.container}>
      <SectionList
        keyExtractor={(item, index) => index.toString()}
        sections={ListData}
        renderItem={renderItem}
        renderSectionHeader={sectionHeader}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#f2c64e",
  },
  listItem: {
    padding: 10,
    fontSize: 14,
  },
  headerItems: {
    backgroundColor: "#947829",
    fontSize: 16,
    color: "black",
    fontWeight: "bold",
    padding: 10,
  },
});

export default SuccessfulScreen;
